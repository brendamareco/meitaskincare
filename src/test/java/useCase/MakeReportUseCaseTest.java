package useCase;

import static org.mockito.Mockito.when;

import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import domain.CompatibilityReportMaker;
import domain.data.CompatibilityReport;
import domain.port.ConditionAnalyzer;
import domain.port.IngredientsScrapper;
import domain.port.OCRGateway;

import reader.JsonReader;

public class MakeReportUseCaseTest 
{
	@Mock
	private ConditionAnalyzer mockedAnalyzer;
	@Mock
	private OCRGateway mockedGateway;
	@Mock
	private IngredientsScrapper mockedScrapper;
	
	private CompatibilityReportMaker reportMaker;
	private MakeReportUseCase makeReportUseCase;
	
	private Set<String>[] listIngredients;
	private Set<String> compatibleIngredients;
	private Set<String> noCompatibleIngredients;
	private Map<String, Boolean>[] analysisResults;
	private Map<String, Boolean> compatibleResult;
	private Map<String, Boolean> noCompatibleResult;
	String conditionName;
	String imgPath;


	@Before
	public void setUp() throws Exception 
	{
		conditionName = "A1";
		imgPath = "imageWithIngredients.png";
		
		JsonReader jsonReader = new JsonReader();
		
		listIngredients = jsonReader.readIngredients("json/ingredients.json");
		compatibleIngredients = listIngredients[0];
		noCompatibleIngredients = listIngredients[1];
		
		analysisResults = jsonReader.readAnalyzerResult("json/analysisResults.json");
		compatibleResult = analysisResults [0];
		noCompatibleResult = analysisResults [1];
		
		mockedGateway = Mockito.mock(OCRGateway.class);
		mockedScrapper = Mockito.mock(IngredientsScrapper.class);
		
		mockedAnalyzer = Mockito.mock(ConditionAnalyzer.class);
		reportMaker = new CompatibilityReportMaker(mockedAnalyzer);
		
		makeReportUseCase = new MakeReportUseCase(reportMaker, mockedGateway, mockedScrapper);
	}

	@Test
	public void successReport() 
	{
		this.setMocks(compatibleIngredients, this.compatibleResult);
		
		CompatibilityReport report = makeReportUseCase.makeReport(imgPath);
		Assert.assertEquals(report.getCondition(), conditionName);
		Assert.assertEquals(report.getIngredients(), this.compatibleIngredients);
		Assert.assertEquals(report.getCompatibilityPerIngrs(), this.compatibleResult);
		Assert.assertTrue(report.isCompatible());		
	}
	
	@Test
	public void failReport() 
	{
		this.setMocks(this.noCompatibleIngredients, this.noCompatibleResult);
		
		CompatibilityReport report = makeReportUseCase.makeReport(imgPath);
		Assert.assertFalse(report.isCompatible());		
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void invalidReport() 
	{
		makeReportUseCase.makeReport("");	
	}
	
	private void setMocks(Set<String> ingredients, Map<String, Boolean> result)
	{
		when(mockedGateway.getData(imgPath))
		.thenReturn(ingredients.toString());
		
		when(mockedScrapper.scrap(ingredients.toString()))
		.thenReturn(ingredients);		
		
		when(mockedAnalyzer.analyze(ingredients))
		.thenReturn(result);
		
		when(mockedAnalyzer.getCondition()).thenReturn(conditionName);
	}

}
