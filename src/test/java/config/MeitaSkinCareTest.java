package config;


import java.io.FileNotFoundException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import useCase.MakeReportUseCase;

public class MeitaSkinCareTest 
{
	private MeitaSkinCare app; 
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();


	@Before	
	public void setUp() throws Exception 
	{
		app = new MeitaSkinCare();
	}

	@Test
	public void getApplication() throws FileNotFoundException 
	{
		MakeReportUseCase useCase = app.initializeMakeReportUseCase();
		
		Assert.assertNotEquals(useCase, null);
	}	
}
