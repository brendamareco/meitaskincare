package reader;


import java.io.InputStreamReader;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import criteriosDeAceptacion.us1.DataSet;

public class JsonReader 
{
	private ClassLoader classLoader;
	
	public JsonReader()
	{
		classLoader = this.getClass().getClassLoader();
	}
	
	public Map<String, Boolean>[] readAnalyzerResult(String path)
	{
		Map<String, Boolean>[] result = null;
		
		InputStreamReader stReader = new InputStreamReader(classLoader.getResourceAsStream(path));  
		
		result = new Gson().fromJson(stReader, new TypeToken<Map<String, Boolean>[]>(){}.getType());

		return result;
	}
		
	public DataSet[] readDataSetUS1(String path)
	{
		DataSet[] dataSets = null;
		
		InputStreamReader stReader = new InputStreamReader(classLoader.getResourceAsStream(path));  
		
		dataSets = new Gson().fromJson(stReader, new TypeToken<DataSet[]>(){}.getType());
		
		return dataSets;		
	}
	
	public Set<String>[] readIngredients(String path)
	{
		Set<String>[] result = null;
		InputStreamReader stReader = new InputStreamReader(classLoader.getResourceAsStream(path));  
		result = new Gson().fromJson(stReader, new TypeToken<Set<String>[]>(){}.getType());
		
		return result;
	}
	
	public String[] readTextResults(String path)
	{
		String[] result = null;
		InputStreamReader stReader = new InputStreamReader(classLoader.getResourceAsStream(path));  
		result = new Gson().fromJson(stReader, new TypeToken<String[]>(){}.getType());
		
		return result;
	}	
}
