package criteriosDeAceptacion.us1;

import java.util.Map;
import java.util.Set;

public class DataSet 
{
	private Set<String> ingredients;
	private Map<String, Boolean> compatibilityPerIngredient;
	
	public DataSet(Set<String> ingredients, Map<String, Boolean> compatibilityPerIngredient) 
	{
		this.ingredients = ingredients;
		this.compatibilityPerIngredient = compatibilityPerIngredient;
	}
	
	public  Set<String> getIngredients()
	{
		return this.ingredients;
	}
	
	public Map<String, Boolean> getCompatibilityPerIngredient()
	{
		return this.compatibilityPerIngredient;
	}
	
}
