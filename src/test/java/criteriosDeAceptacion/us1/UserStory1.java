package criteriosDeAceptacion.us1;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import domain.CompatibilityReportMaker;
import domain.data.CompatibilityReport;
import domain.port.ConditionAnalyzer;
import reader.JsonReader;

public class UserStory1 
{
	@Mock
	private ConditionAnalyzer mockedAnalyzer;

	private DataSet[] dataSets;
	private DataSet dataSetTodosCompatibles;
	private DataSet dataSetNingunoCompatible;
	private DataSet dataSetCompatibleYnoCompatible;
	private DataSet dataSetMayoriaNoCompatibles;
	private DataSet dataSetMayoriaCompatibles;
	private DataSet dataSetVacio;	
	private CompatibilityReportMaker reportMaker;

	@Before
	public void setUp() throws Exception 
	{		
		JsonReader jsonReader = new JsonReader();

		dataSets = jsonReader.readDataSetUS1("json/dataSetsUS1.json");
		dataSetTodosCompatibles = dataSets[0];
		dataSetNingunoCompatible = dataSets[1];
		dataSetCompatibleYnoCompatible = dataSets[2];
		dataSetMayoriaNoCompatibles = dataSets[3];
		dataSetMayoriaCompatibles = dataSets[4];
		dataSetVacio = new DataSet(new HashSet<String>(), new HashMap<String, Boolean>());
		
		mockedAnalyzer = Mockito.mock(ConditionAnalyzer.class);
		reportMaker = new CompatibilityReportMaker(mockedAnalyzer);
	}

	@Test
	public void CA1() 
	{
		this.setMockedAnalyzer(dataSetTodosCompatibles);
		
		CompatibilityReport report = reportMaker.makeReport(dataSetTodosCompatibles.getIngredients());
			
		Assert.assertTrue(report.isCompatible());
	}
	
	@Test
	public void CA2() 
	{
		setMockedAnalyzer(dataSetNingunoCompatible);
		
		CompatibilityReport report = reportMaker.makeReport(dataSetNingunoCompatible.getIngredients());
			
		Assert.assertFalse(report.isCompatible());
	}
	
	@Test
	public void CA3() 
	{
		setMockedAnalyzer(dataSetCompatibleYnoCompatible);
		
		CompatibilityReport report = reportMaker.makeReport(dataSetCompatibleYnoCompatible.getIngredients());
			
		Assert.assertFalse(report.isCompatible());
	}
	
	@Test
	public void CA4() 
	{
		setMockedAnalyzer(dataSetMayoriaNoCompatibles);
		
		CompatibilityReport report = reportMaker.makeReport(dataSetMayoriaNoCompatibles.getIngredients());
			
		Assert.assertFalse(report.isCompatible());
	}
	
	@Test
	public void CA5() 
	{
		setMockedAnalyzer(dataSetMayoriaCompatibles);
				
		CompatibilityReport report = reportMaker.makeReport(dataSetMayoriaCompatibles.getIngredients());
			
		Assert.assertTrue(report.isCompatible());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void CA6() 
	{
		setMockedAnalyzer(dataSetVacio);
		
		reportMaker.makeReport(dataSetVacio.getIngredients());
	}
	
	private void setMockedAnalyzer(DataSet dataSet)
	{
		Set<String> ingredients = dataSet.getIngredients();
		Map<String, Boolean> expectedResult = dataSet.getCompatibilityPerIngredient();
		when(mockedAnalyzer.analyze(ingredients)).thenReturn(expectedResult);
	}
}
