package criteriosDeAceptacion;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.io.File;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;

import domain.exceptions.ImageNotFoundException;
import domain.exceptions.InvalidFormatException;
import domain.exceptions.TextNotFoundException;
import infrastructure.TesseractGateway;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.TesseractException;

public class UserStory3 
{
	private String folder;
	private String imageWithText;
	private String imageWithOutText;
	private String notImageFile;
	private String UnknowFile;
	private TesseractGateway tesseractGateway;

	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Mock
	private ITesseract tesseract;
	
	@Before
	public void setUp() throws Exception 
	{
		folder = "src/test/resources/us3/";
		imageWithText = "Imagen.png";
		imageWithOutText = "whiteImage.png";
		notImageFile = "text.txt";
		UnknowFile = "unknow";
		tesseract = Mockito.mock(ITesseract.class);
		tesseractGateway = new TesseractGateway(tesseract);
	}

	@Test
	public void CA1() throws TesseractException
	{
		String imgPath =getImagePath (this.imageWithText);
		String expectedResult = "Informe PPII";
		
		this.setMock(imgPath, expectedResult);
		
		String tesseractResult = tesseractGateway.getData(imgPath);
		
		assertEquals(tesseractResult, expectedResult);		
	}
	
	@Test (expected = TextNotFoundException.class)
	public void CA2() throws TesseractException
	{
		String imgPath = getImagePath (this.imageWithOutText);
	
		this.setMock(imgPath, "");
		
		tesseractGateway.getData(imgPath);	
	}
	
	@Test (expected = InvalidFormatException.class)
	public void CA3() throws TesseractException
	{
		String imgPath = getImagePath(this.notImageFile);
				
		when(tesseract.doOCR(new File(imgPath))).thenThrow(new TesseractException());
		
		tesseractGateway.getData(imgPath);	
	}
	
	@Test (expected = ImageNotFoundException.class)
	public void CA4() throws TesseractException
	{	
		tesseractGateway.getData(getImagePath(this.UnknowFile));	
	}
	
	private String getImagePath(String filePath)
	{
		return folder + filePath;
	}
	
	private void setMock (String arg, String expectedResult)
	{
		try 
		{
			when(tesseract.doOCR(new File(arg))).thenReturn(expectedResult);
		} catch (TesseractException e) { e.printStackTrace(); }
	}

}
