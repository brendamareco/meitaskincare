package criteriosDeAceptacion;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import config.finder.AnalyzersFinder;
import config.finder.FileLocator;
import config.finder.IDEFileLocator;

public class UserStory2 
{
	private String folder;
	private String dirNoImplemetation;
	private String dirOneImplemetation;
	private String dirTwoImplemetation;
	private String unknowDir;
	private AnalyzersFinder finder;
	
	@Before
	public void setUp()
	{
		FileLocator fileLocator = new IDEFileLocator();
		finder = new AnalyzersFinder(fileLocator);
		
		folder = "src/test/resources/";
		dirNoImplemetation = "dirNoImplementations";
		dirOneImplemetation = "dirOneImplementation";
		dirTwoImplemetation = "dirTwoImplementations";
		unknowDir = "notADir";
	}

	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test 
	public void CA1() throws FileNotFoundException 
	{	
		thrown.expect(FileNotFoundException.class);
	
		finder.find(folder + dirNoImplemetation);
	}

	@Test
	public void CA2() 
	{	
		int listSize = sizeOfFinder(folder + dirOneImplemetation);
		assertEquals(listSize,1);	
	}
	
	@Test
	public void CA3() 
	{	
		int listSize = sizeOfFinder(folder + dirTwoImplemetation);
		assertEquals(listSize,2);	
	}
	
	@Test
	public void CA4() throws  FileNotFoundException 
	{
		thrown.expect(FileNotFoundException.class);	
		
		finder.find(folder + unknowDir);	
	}
	
	private int sizeOfFinder(String path)
	{	
		try { return finder.find(path).size(); }
		catch ( FileNotFoundException e) { return 0; }
	}
}
