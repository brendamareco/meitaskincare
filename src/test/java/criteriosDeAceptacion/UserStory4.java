package criteriosDeAceptacion;

import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import domain.exceptions.IngredientsNotFoundException;
import infrastructure.UniversalIngredientsScrapper;
import reader.JsonReader;

public class UserStory4 
{
	private UniversalIngredientsScrapper scrapper;
	private String oneIngredient;
	private String withSpecialCharacters;
	private String withParenthesis;
	private String emptyString;
	private String withoutIngredients;
	private String noStart; 
	private String noEnd;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Before
	public void setUp() throws Exception 
	{
		JsonReader reader = new JsonReader();
		String[] textResults = reader.readTextResults("json/textResultsUS4.json");
		
		oneIngredient = textResults[0];
		withSpecialCharacters = textResults[1];
		withParenthesis = textResults[2];
		emptyString = textResults[3];
		withoutIngredients = textResults[4];
		noStart = textResults[5]; 
		noEnd = textResults[6];
		
		String start = "ingredientes";
		String end = ".";
		scrapper = new UniversalIngredientsScrapper(start, end);
	}

	@Test
	public void CA1() 
	{
		Set<String> expectedResult = Set.of("aqua");
		Set<String> result = scrapper.scrap(oneIngredient);
		
		Assert.assertEquals(result.size(), 1);
		Assert.assertEquals(result, expectedResult);
	}
	
	@Test
	public void CA2() 
	{
		Set<String> expectedResult = Set.of("quatermium-80" ,"alcohol");
		Set<String> result = scrapper.scrap(this.withSpecialCharacters);
		
		Assert.assertEquals(result.size(), 2);
		Assert.assertTrue(result.containsAll(expectedResult));
	}
	
	@Test
	public void CA3() 
	{
		Set<String> expectedResult = Set.of("aqua");
		Set<String> result = scrapper.scrap(this.withParenthesis);
		
		Assert.assertEquals(result.size(), 1);
		Assert.assertEquals(result, expectedResult);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void CA4() 
	{
		scrapper.scrap(this.emptyString);
	}
	
	@Test (expected = IngredientsNotFoundException.class)
	public void CA5()  
	{
		scrapper.scrap(this.withoutIngredients);
	}
	
	@Test (expected = UnsupportedOperationException.class)
	public void CA6() 
	{
		scrapper.scrap(this.noStart);
	}
	
	@Test (expected = UnsupportedOperationException.class)
	public void CA7() 
	{
		scrapper.scrap(this.noEnd);
	}
}