package domain;

import java.util.Map;
import java.util.Set;

import domain.data.CompatibilityReport;
import domain.port.ConditionAnalyzer;

public class CompatibilityReportMaker 
{
	private ConditionAnalyzer analyzer;
	
	public CompatibilityReportMaker(ConditionAnalyzer analyzer)
	{
		this.analyzer = analyzer;
	}
	
	public CompatibilityReport makeReport(Set<String> ingrs)
	{
		if(ingrs.isEmpty())
			throw new IllegalArgumentException("No se ingresaron ingredientes");
		
		Map<String, Boolean> compatibilityPerIngrs = analyzer.analyze(ingrs);
		String condition = analyzer.getCondition();
		boolean isRecommended;
		float cantIngredientes = compatibilityPerIngrs.size();
		float cantIngredientesCompatibles = 0;

		for (Map.Entry<String, Boolean> entry : compatibilityPerIngrs.entrySet())
		{
			if(entry.getValue())
				cantIngredientesCompatibles ++;
		}
		
		isRecommended = (cantIngredientesCompatibles/cantIngredientes) > 0.5;
		
		return new CompatibilityReport(condition, ingrs, isRecommended, compatibilityPerIngrs);
	}
}
