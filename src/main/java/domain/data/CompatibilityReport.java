package domain.data;

import java.util.Map;
import java.util.Set;

public class CompatibilityReport 
{
	private String condition;
	private Set<String> ingredients;
	private boolean isCompatible;
	private Map<String, Boolean> compatibilityPerIngrs;
	
	public CompatibilityReport(String condition, Set<String> ingredients, boolean isCompatible,
				  				Map<String, Boolean> compatibilityPerIngrs)
	{
		this.condition = condition;
		this.ingredients = ingredients;
		this.isCompatible = isCompatible;
		this.compatibilityPerIngrs = compatibilityPerIngrs;
	}

	public String getCondition() 
	{
		return condition;
	}

	public Set<String> getIngredients() 
	{
		return ingredients;
	}

	public boolean isCompatible() 
	{
		return isCompatible;
	}

	public Map<String, Boolean> getCompatibilityPerIngrs() 
	{
		return compatibilityPerIngrs;
	}
}
