package domain.port;

import java.util.Map;
import java.util.Set;

public interface ConditionAnalyzer 
{
	public Map<String, Boolean> analyze(Set<String> ingredients);
	public String getCondition();
}
