package domain.port;

import java.util.Set;

public interface IngredientsScrapper 
{
	public Set<String> scrap(String text);
}
