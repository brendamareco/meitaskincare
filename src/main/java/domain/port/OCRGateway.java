package domain.port;

public interface OCRGateway 
{
	public String getData(String imgPath);
}
