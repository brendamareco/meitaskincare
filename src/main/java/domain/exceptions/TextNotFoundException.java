package domain.exceptions;

public class TextNotFoundException extends IllegalArgumentException
{

	private static final long serialVersionUID = 1L;
	
	public TextNotFoundException(String message)
	{
		super(message);
	}
}
