package domain.exceptions;

public class IngredientsNotFoundException extends IllegalArgumentException
{
	private static final long serialVersionUID = 1L;
	
	public IngredientsNotFoundException(String message)
	{
		super(message);
	}

}
