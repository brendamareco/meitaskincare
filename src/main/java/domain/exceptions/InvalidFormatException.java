package domain.exceptions;

public class InvalidFormatException extends IllegalArgumentException
{

	private static final long serialVersionUID = 1L;
	
	public InvalidFormatException(String message)
	{
		super(message);
	}
}
