package domain.exceptions;

public class ImageNotFoundException extends IllegalArgumentException
{

	private static final long serialVersionUID = 1L;
	
	public ImageNotFoundException(String message)
	{
		super(message);
	}
}
