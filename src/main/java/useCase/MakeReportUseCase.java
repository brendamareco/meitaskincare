package useCase;

import java.util.Set;

import domain.CompatibilityReportMaker;
import domain.data.CompatibilityReport;
import domain.port.IngredientsScrapper;
import domain.port.OCRGateway;

public class MakeReportUseCase 
{
	private OCRGateway ocrGateway;
	private IngredientsScrapper ingrsScrapper;
	private CompatibilityReportMaker reportMaker;
	
	public MakeReportUseCase(CompatibilityReportMaker reportMaker, OCRGateway ocrGateway, 
							 IngredientsScrapper ingrsScrapper)
	{
		this.reportMaker = reportMaker;
		this.ocrGateway = ocrGateway;
		this.ingrsScrapper = ingrsScrapper;
	}
	
	public CompatibilityReport makeReport(String imgPath)
	{
		String imgText = ocrGateway.getData(imgPath);
		Set<String> ingrs = ingrsScrapper.scrap(imgText);
		return reportMaker.makeReport(ingrs);
	}
}
