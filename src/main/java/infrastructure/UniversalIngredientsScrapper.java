package infrastructure;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import domain.exceptions.IngredientsNotFoundException;
import domain.port.IngredientsScrapper;

public class UniversalIngredientsScrapper implements IngredientsScrapper
{
	private String startKey;
	private String endKey;
	
	public UniversalIngredientsScrapper(String startKey, String endKey)
	{
		this.startKey = startKey;
		this.endKey = endKey;
	}

	@Override
	public Set<String> scrap(String text) 
	{
		if (text.isEmpty())
			throw new IllegalArgumentException("El texto se encuentra vacio.");
		
		Set<String> ingredients = new HashSet<String>();
		int startPos = text.indexOf(startKey) + startKey.length();
		int endPos = text.indexOf(endKey, startPos);
		
		if (startPos == -1 || endPos == -1)
			throw new  UnsupportedOperationException("No se detecto el identificador de inicio o final");
		
		String boundaries = text.substring(startPos, endPos);
		boundaries = boundaries.replaceAll("[*\n~/<]", "");
		
		if (boundaries.isEmpty())
			throw new IngredientsNotFoundException("No se identificaron ingredientes en el texto ");
		
		Set<String> words = new HashSet<String>(Arrays.asList(boundaries.split(",")));
		
		words.forEach(word -> 
		{
			String ingredient = word.replaceAll("\\(.*\\)", "");
			
	    	ingredient = ingredient.strip();
	    	ingredients.add(ingredient);
		});
		
		return ingredients;
	}

}
