package infrastructure;

import java.io.File;
import domain.exceptions.ImageNotFoundException;
import domain.exceptions.InvalidFormatException;
import domain.exceptions.TextNotFoundException;
import domain.port.OCRGateway;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.LoadLibs;

public class TesseractGateway implements OCRGateway
{
	private ITesseract tesseract;
	
	public TesseractGateway(ITesseract tesseract)
	{
		this.tesseract = tesseract;
		File tessDataFolder = LoadLibs.extractTessResources("tessdata");
		tesseract.setDatapath(tessDataFolder.getAbsolutePath());
	}
	
	@Override
	public String getData(String imgPath) 
	{
		String result = "";
		File imageFile = new File(imgPath);

		if (!imageFile.exists())
			throw new ImageNotFoundException ("No existe la imagen en el directorio");
				
		try
		{ result = tesseract.doOCR(imageFile); } 
		catch (TesseractException e)
		{ throw new InvalidFormatException ("No se pudo obtener el texto del archivo."); }
		
		if(result.isEmpty())
			throw new TextNotFoundException("La imagen no contiene texto");
	
		return result; 		
	}
}
