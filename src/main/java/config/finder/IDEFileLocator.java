package config.finder;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.Set;

public class IDEFileLocator implements FileLocator
{
	private ClassLoader classLoader;

	@Override
	public Set<File> getClassFiles(String path) throws FileNotFoundException 
	{
		Set<File> classFiles =  new HashSet<>();
		File dir = new File(path);
		
		if( !dir.exists())
			throw new FileNotFoundException ("El directorio no existe") ;
		
		try
		{
			classLoader = new URLClassLoader( new URL[] {dir.getParentFile().toURI().toURL()});
		}
		catch (MalformedURLException e) { e.printStackTrace(); }
		
		for (File file : dir.listFiles()) 
		{
			if (!file.getName().endsWith(".class")) continue;
			
			classFiles.add(file);
		}
		
		return classFiles;
	}

	@Override
	public ClassLoader getClassLoader() 
	{
		return this.classLoader;
	}

}
