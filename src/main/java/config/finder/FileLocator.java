package config.finder;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Set;

public interface FileLocator 
{
	public Set<File> getClassFiles(String path) throws FileNotFoundException;
	public ClassLoader getClassLoader();
}
