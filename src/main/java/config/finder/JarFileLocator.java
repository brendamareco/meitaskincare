package config.finder;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JarFileLocator implements FileLocator
{
	private ClassLoader classLoader;
	private String jarPath;
	
	public JarFileLocator()
	{
		try 
		 {
			this.jarPath = getClass().getProtectionDomain()
			            .getCodeSource()
			            .getLocation()
			            .toURI()
			            .getPath();
		}
		catch (URISyntaxException e) { e.printStackTrace();}
	}
	
	@Override
	public Set<File> getClassFiles(String path)
	{
		Set<File> classFiles =  new HashSet<>();
		classLoader = getClass().getClassLoader();
					
	    List<Path> filesPath;
	    Stream<Path> walk = getJarPathFromFolder(path);
	    
	    if (walk == null)
	    	throw new NullPointerException ("El path del archivo no debe ser null");
	    
	    filesPath = walk
	                    .filter(Files::isRegularFile)
	                    .filter(p -> p.getFileName().toString().endsWith(".class"))
	                    .collect(Collectors.toList());
	        	
			for (Path p : filesPath) 
	        {
	            classFiles.add(new File(p.toString()));            
	        }
	  
				
		return classFiles;
	}

	@Override
	public ClassLoader getClassLoader() 
	{
		return this.classLoader;
	}
	
	public Stream<Path> getJarPathFromFolder(String folder) 
	{
		URI uri = URI.create("jar:file:" + jarPath);
		FileSystem fs = null;
		try 
		{
			fs = FileSystems.newFileSystem(uri, Collections.emptyMap());
			return Files.walk(fs.getPath(folder));
		} catch (IOException e) { e.printStackTrace(); }
		
		return null;
	}
}
