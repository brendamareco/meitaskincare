package config.finder;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

import domain.port.ConditionAnalyzer;

public class AnalyzersFinder 
{
	private FileLocator fileLocator;
	
	public AnalyzersFinder(FileLocator fileLocator)
	{
		this.fileLocator = fileLocator; 
	}
	
	public  Set<ConditionAnalyzer> find(String path) throws FileNotFoundException
	{
		Set<File> classFiles = fileLocator.getClassFiles(path);
		ClassLoader classLoader = fileLocator.getClassLoader();
		
		Set<ConditionAnalyzer> analyzerImplementations = new HashSet<>();
		
		for (File file : classFiles)
		{
			try 
			{
				//Cargamos la clase
				Class<?> c = classLoader.loadClass( getFQN(file) );
				//Verificamos que sea un Analyzer
				if (!ConditionAnalyzer.class.isAssignableFrom(c)) continue;
				
				ConditionAnalyzer analyzer = (ConditionAnalyzer) c.getDeclaredConstructor().newInstance();
				analyzerImplementations.add(analyzer);			
				
			}
			catch (Exception e) { e.printStackTrace(); }					
		}
		
		if (analyzerImplementations.isEmpty())
			throw new FileNotFoundException ("No existen implementaciones de la interfaz ConditionAnalyzer en el directorio");
			
		return analyzerImplementations;
	}
	
	private String getFQN(File file)
	{
		String parentName = file.getParent().replace("/", "\\");
		String packageName =  parentName.substring(parentName.lastIndexOf("\\")+1, parentName.length());
		String fileName = file.getName().replace(".class", "");
		
		return packageName + "." + fileName;
	}
	
}
