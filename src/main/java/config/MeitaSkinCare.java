package config;

import java.io.FileNotFoundException;
import java.util.Set;

import config.finder.AnalyzersFinder;
import config.finder.FileLocator;
import config.finder.IDEFileLocator;
import config.finder.JarFileLocator;
import domain.CompatibilityReportMaker;
import domain.port.ConditionAnalyzer;
import domain.port.IngredientsScrapper;
import domain.port.OCRGateway;
import infrastructure.TesseractGateway;
import infrastructure.UniversalIngredientsScrapper;
import net.sourceforge.tess4j.Tesseract;
import useCase.MakeReportUseCase;

public class MeitaSkinCare 
{
	private String analyzersFolder;
	private AnalyzersFinder analyzersFinder;

	public MeitaSkinCare()
	{
		String context = this.getClass().getResource("").getProtocol();
		setFinderByContext(context);
	}

	private void setFinderByContext(String context) 
	{
		FileLocator fileLocator;
		if(context.equals("file"))
		{
			fileLocator = new IDEFileLocator();	
			this.analyzersFolder = "plugin/analyzer";
		}
		else 
		{ 
			fileLocator = new JarFileLocator();
			this.analyzersFolder = "analyzer";
		}
			
		this.analyzersFinder = new AnalyzersFinder(fileLocator);
	}
	
	public MakeReportUseCase initializeMakeReportUseCase() 
	{
		Set<ConditionAnalyzer> analyzers;
		try 
		{
			analyzers = analyzersFinder.find(analyzersFolder);
			ConditionAnalyzer analyzer = analyzers.iterator().next();
			CompatibilityReportMaker reportMaker = new CompatibilityReportMaker(analyzer);
			OCRGateway ocrGateway = new TesseractGateway(new Tesseract());
			IngredientsScrapper ingrsScrapper = new UniversalIngredientsScrapper("ingredientes: ",".");
			return new MakeReportUseCase(reportMaker, ocrGateway, ingrsScrapper);
		}
		catch (FileNotFoundException e) {return null;}
	}
}
